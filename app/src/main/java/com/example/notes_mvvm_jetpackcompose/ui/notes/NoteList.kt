package com.example.notes_mvvm_jetpackcompose.ui.notes

import android.graphics.drawable.Icon
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Home
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import androidx.compose.runtime.livedata.observeAsState
import androidx.navigation.NavHostController
import com.example.notes_mvvm_jetpackcompose.presentation.NoteViewModel


@Composable
fun NoteList(navigationController: NavHostController, noteViewModel: NoteViewModel?) {

    val text by noteViewModel?.textModel!!.observeAsState(initial = "")

    Scaffold(topBar = { NoteListToolbar()}) {
        Text(text = text, modifier = Modifier
            .fillMaxWidth()
            .height(50.dp)
            .clickable {
                noteViewModel?.randomText()
                //navigationController.navigate("noteEdit")
            })

    }
}


@Composable
fun NoteListToolbar(){
    TopAppBar(title = { Text("Notas", color = Color.White) }, 
       backgroundColor = Color.Blue,
       contentColor = Color.White,
        navigationIcon = {
            IconButton(onClick = { /*TODO*/ }) {
                Icon(imageVector = Icons.Filled.Home, contentDescription = "Home")
            }
        }
    ) 
}