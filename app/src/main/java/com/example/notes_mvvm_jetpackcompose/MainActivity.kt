package com.example.notes_mvvm_jetpackcompose

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavHost
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.example.notes_mvvm_jetpackcompose.app.Routes
import com.example.notes_mvvm_jetpackcompose.presentation.NoteViewModel
import com.example.notes_mvvm_jetpackcompose.presentation.NoteViewModelFactory
import com.example.notes_mvvm_jetpackcompose.ui.notedetails.NoteDetail
import com.example.notes_mvvm_jetpackcompose.ui.notedetails.NoteEdit
import com.example.notes_mvvm_jetpackcompose.ui.notes.NoteList
import com.example.notes_mvvm_jetpackcompose.ui.theme.Notes_MVVM_JetpackComposeTheme

class MainActivity : ComponentActivity() {
    private val viewModel by viewModels<NoteViewModel> {
        NoteViewModelFactory()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            Notes_MVVM_JetpackComposeTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colors.background
                ) {
                    var navigationController = rememberNavController()
                    NavHost(navigationController, startDestination = "noteList") {
                            composable ( Routes.NoteList.route){ NoteList(navigationController, viewModel) }
                            composable( Routes.NoteDetail.route){ NoteDetail(navigationController, viewModel) }
                            composable(Routes.NoteEdit.route ){ NoteEdit(navigationController, viewModel) }
                    }

                }
            }
        }
    }
}

@Composable
fun Greeting(name: String) {
    Text(text = "Hello $name!")
}

@Preview(showBackground = true)
@Composable
fun DefaultPreview() {
    Notes_MVVM_JetpackComposeTheme {
        var navigationController = rememberNavController()
        Surface(
            modifier = Modifier.fillMaxSize(),
            color = MaterialTheme.colors.background
        ) {
            NoteList(navigationController, viewModel())
        }
    }
}