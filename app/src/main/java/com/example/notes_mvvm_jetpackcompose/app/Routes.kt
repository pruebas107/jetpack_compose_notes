package com.example.notes_mvvm_jetpackcompose.app

sealed class Routes(val route: String) {
    object NoteList:Routes("noteList")
    object NoteDetail:Routes("noteDetail")
    object NoteEdit:Routes("noteEdit")
}